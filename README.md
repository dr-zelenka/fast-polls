# Fast Polls

Fast Polls is super easy system for your polls! Is based on PHP, JS, MySQL and using Smarty templates system.

## Getting Started

### What you need?

You need own www server to run code like server, VPS or webhosting. Also you need a place where put database.

### Installing

#### 1. Download code
#### 2. Upload database.sql to your database
#### 3. Change *config.php* file (*includes/config.php*)

```
$database['host']			      = 'localhost'; // Database server
$database['port']			      = '3306'; // Port, default is 3306
$database['user']			      = 'root'; // User
$database['userpw']			      = 'password'; // Password
$database['databasename']	      = 'poll'; // Database name
$database['tableprefix']          = ''; // Table prefix, by default, tables doesn't have a prefix
```

#### 4. Done!

### Configuration

Basic configuration do in *constants.php* file (*includes/constants.php*) after comment *//Editable* on line 16

#### Language support

Fast Polls have basic support for multiple languages. Default is EN, it's necessary don't delete or remove this lang...
Also there is a czech, beacause thats my primary language. :)

You can easy translate file and create new folder with them. After that, you must add your new language to array in this file.

#### ReCAPTCHA

Fast Polls have a antibot protection by ReCAPTCHA. If you want to use it, you must create your own ReCAPTCHA account.
Change *RC_ON* to true. And put public and private key.

That's all for now. Thanks for using!