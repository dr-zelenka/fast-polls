<?php

class Database {
	public $connection;
	public $result;
	private $config;

	public function __construct()
	{
		$this->config = $GLOBALS['database'];
		$this->connect();
	}

	public function __destruct()
	{
		mysqli_close($this->connection);
	}

	private function connect()
	{

		$this->connection = new mysqli($this->config['host'], $this->config['user'], $this->config['userpw'], $this->config['databasename']);

		mysqli_set_charset($this->connection,"utf8");

		if(mysqli_connect_error()) {
			return "Connection to database failed: ".mysqli_connect_error();
		}
	}

	public function query($query)
	{
		//$this->errorReport($query);
		$this->result = $this->connection->query($query);

		return $this->result;
	}

	public function getFirstRow($query)
	{
		//$this->errorReport($query);
		$query = $this->query($query);
		$this->result = mysqli_fetch_assoc($query);

		return $this->result;
	}

	public function fetch_array($query) {
		return $query->fetch_array(MYSQLI_ASSOC);
	}

	public function free_result($query)
	{
		return mysqli_free_result($query);
	}

	public function GetInsertID()
	{
		return mysqli_insert_id($this->connection);
	}

	public function errorReport($query)
	{
		if (!mysqli_query($this->connection, $query)) {
    	printf("Database Error: %s; %s", $query, mysqli_error($this->connection));

			exit;
		}
	}

}
