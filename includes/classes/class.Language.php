<?php

class Language implements ArrayAccess {
  private $container = array();

  public function __construct()
  {
    $this->getFiles();
  }

  public function getFiles()
  {
    require('language/'.$this->getLanguage().'/main.php');
    $this->addData($LNG);
  }

  public function setLanguage($language) {
    $_SESSION["language"] = $language;
  }

  public function getLanguage() {
    if(empty($_SESSION["language"])) {
      $_SESSION["language"] = 'en';
    }

    return $_SESSION["language"];
  }

  public function addData($data) {
    $this->container = array_replace_recursive($this->container, $data);
  }

  public function getLanguages() {
    return LANG_SUPPORT;
  }

	/** ArrayAccess Functions **/

  public function offsetSet($offset, $value) {
      if (is_null($offset)) {
          $this->container[] = $value;
      } else {
          $this->container[$offset] = $value;
      }
  }

  public function offsetExists($offset) {
      return isset($this->container[$offset]);
  }

  public function offsetUnset($offset) {
      unset($this->container[$offset]);
  }

  public function offsetGet($offset) {
      return isset($this->container[$offset]) ? $this->container[$offset] : $offset;
  }

}
