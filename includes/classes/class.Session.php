<?php

class Session
{
	static public function start() {
		session_start();
	}
	
	static public function create($id) {
		
		if (empty($_SESSION["id"])) {
			$_SESSION["id"] = $id;
		}
		
	}
	
	static public function isExist() {
	
		if(!empty($_SESSION["id"])) {
			return true;
		} else {
			return false;
		}
	}
	
	static public function destroy() {
	
		if(!empty($_SESSION["id"])) {
			session_destroy();
		}
	}
}