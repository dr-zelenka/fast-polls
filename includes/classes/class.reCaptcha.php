<?php

class reCaptcha
{

	private $google_url = "https://www.google.com/recaptcha/api/siteverify";
	private $capprivate = RC_SECRET_KEY;

    /* Google recaptcha API url */

    public function VerifyCaptcha($response)
    {

        $url = $this->google_url."?secret=".$this->capprivate.
               "&response=".$response;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_TIMEOUT, 15);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, TRUE);
        //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, TRUE);
        $curlData = curl_exec($curl);

        curl_close($curl);

        $res = json_decode($curlData, TRUE);
        if($res['success'] == 'true')
            return TRUE;
        else
            return FALSE;
    }

}
