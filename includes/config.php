<?php

$database	= array();

$database['host']			      = 'localhost';  // Database server
$database['port']			      = '3306';       // Port, default is 3306
$database['user']			      = 'root';       // User
$database['userpw']			    = 'password';   // Password
$database['databasename']	  = 'poll';       // Database name
$database['tableprefix']    = '';           // Table prefix, by default, tables doesn't have a prefix

?>
