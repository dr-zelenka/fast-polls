<?php

define('HTTPS'						, isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]  == 'on');
define('PROTOCOL'					, HTTPS ? 'https://' : 'http://');

define('HTTP_BASE'					, str_replace(array('\\', '//'), '/', dirname($_SERVER['REQUEST_URI']).'/'));
define('HTTP_ROOT'					, str_replace(basename($_SERVER['SCRIPT_FILENAME']), '', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));

define('HTTP_FILE'					, basename($_SERVER['SCRIPT_NAME']));
define('HTTP_HOST'					, $_SERVER['HTTP_HOST']);
define('HTTP_PATH'					, PROTOCOL.HTTP_HOST.HTTP_ROOT);

define('UTF8_SUPPORT'				, true);


// Editable

// Define multilang
// MOTE: EN is default language! If you can delete or rename EN folder, you get many errors...
define('LANG_SUPPORT'				, array('en', 'cz'));

// ReCAPTCHA setting
define('RC_ON'				      	, false);
define('RC_PUBLIC_KEY'				, "");
define('RC_SECRET_KEY'      		, "");
