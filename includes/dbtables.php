<?php

define('DB_NAME'			, $database['databasename']);
define('DB_PREFIX'		, $database['tableprefix']);

define('ANSWERS'			, DB_PREFIX.'answers');
define('POLLS'				, DB_PREFIX.'polls');
define('VOTES'				, DB_PREFIX.'votes');
