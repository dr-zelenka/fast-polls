<?php

abstract class AbstractIndexPage
{

	protected $window;
	public $defaultWindow = 'normal';

	protected function __construct() {
		$this->setWindow($this->defaultWindow);
	}

	protected function setWindow($window) {
		$this->window	= $window;
	}

	protected function getWindow() {
		return $this->window;
	}

	protected function render($file) {

		global $smarty, $LNG;

		$smarty->assign(array(
			'LNG' 						=> $LNG,
			'languages' 			=> $LNG->getLanguages(),
			'captcha_setting' => RC_ON,
			'captcha_public' 	=> RC_PUBLIC_KEY
		));

		$smarty->setTemplateDir('styles/templates/index/');

		$smarty->display('extends:layout.'.$this->getWindow().'.tpl|'.$file);
		//$smarty->display('extends:styles/templates/index/layout.normal.tpl|styles/templates/index/'.$file);
		exit;
	}

	protected function printMessage($title, $message, $redirect = NULL, $redirectButtons = NULL, $fullSide = true)
	{
		global $smarty;

		$smarty->assign(array(
			'title' 						=> $title,
			'message'						=> $message,
			'redirectButtons'		=> $redirectButtons,
		));

		if(!empty($redirect)) {
			header('Refresh: '.$redirect[0].'; url='.$redirect[1]);
			exit;
		}

		if($fullSide) {
			$this->setWindow('normal');
		}

		$this->render('error.message.tpl');
	}

	protected function redirect($url, $time = 0) {
		header('Refresh: '.$time.'; url='.$url);
		exit;
	}

}
