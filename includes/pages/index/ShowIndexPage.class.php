<?php

class ShowIndexPage extends AbstractIndexPage
{
	function __construct()
	{
		parent::__construct();
	}

	function show()
	{
		$this->render('page.index.default.tpl');
	}

	function send() {

		global $LNG, $recaptcha;

		$question 	= GeneralFunctions::_GP('question', '');
		$answer 		= $_POST['answer'];

		$errors = array();

		if(empty($question)) {
			$errors[] = $LNG['error_question'];
		}

		if(empty(array_filter($answer))) {
			$errors[] = $LNG['error_answer'];
		}

		if(RC_ON == TRUE) {
			$recaptcha_result = $recaptcha->VerifyCaptcha($_POST['g-recaptcha-response']);

			if($recaptcha_result != TRUE) {
				$errors[] = $LNG['error_recaptcha'];
			}
		}

		if(!empty($errors)) {
			$this->printMessage(
				$LNG['error'],
				implode("<br>\r\n", $errors),
				NULL, array(array('label'	=> $LNG['error_back'], 'url'	=> 'javascript:window.history.back()',))
			);
		}

		$GLOBALS['DATABASE']->query("INSERT INTO ".POLLS." (`question`) VALUES ('".$question."')");
		$id = $GLOBALS['DATABASE']->GetInsertID();

	  for($i = 0; $i < count($answer); $i++)
	  {
			$order = $i + 1;

			$GLOBALS['DATABASE']->query("INSERT INTO ".ANSWERS." (`id_poll`, `answer_order`, `answer`) VALUES ('".$id."', '".$order."', '".$answer[$i]."')");
	  }


		GeneralFunctions::redirectTo('index.php?page=poll&id='.$id);
	}

}
