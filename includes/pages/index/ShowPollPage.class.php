<?php

class ShowPollPage extends AbstractIndexPage
{
	function __construct()
	{
		parent::__construct();
	}

	function show()
	{
		global $smarty, $LNG;

		$id = GeneralFunctions::_GP('id', 0);

		$query  	= $GLOBALS['DATABASE']->query("SELECT COUNT(*) AS count FROM ".POLLS." WHERE `id_poll` = ".$id."");
		$result 	= $GLOBALS['DATABASE']->fetch_array($query);

		if($result['count'] == 0) {
			$this->printMessage($LNG['error'], $LNG['poll_not_found'], NULL, array(array('label'	=> $LNG['error_back'], 'url'	=> 'javascript:window.history.back()',)));
		}

		$query = $GLOBALS['DATABASE']->query("SELECT * FROM ".ANSWERS." WHERE `id_poll` = '".$id."' ORDER BY `answer_order`");
		$array = array();

		while($answers = $GLOBALS['DATABASE']->fetch_array($query)) {
			$array[$answers['answer_order']] = $answers;
		}

		$question = $GLOBALS['DATABASE']->getFirstRow("SELECT * FROM ".POLLS." WHERE `id_poll` = '".$id."'");

		$url = HTTP_PATH.HTTP_FILE."?page=poll&id=".$id;

		$smarty->assign(array(
			'id' 					=> $id,
			'poll_url' 		=> $url,
			'array' 			=> $array,
			'question' 		=> $question['question']
		));

		$this->render('page.poll.default.tpl');
	}

	function send()
	{
			global $LNG, $recaptcha;

			$id 		= GeneralFunctions::_GP('id', 0);
			$answer = GeneralFunctions::_GP('answer', '');

			$errors = array();

			if(empty($answer)) {
				$errors[] = $LNG['poll_not_answer'];
			}

			if($id == 0) {
				$errors[] = $LNG['poll_id_error'];
			}

			if(RC_ON == TRUE) {
				$recaptcha_result = $recaptcha->VerifyCaptcha($_POST['g-recaptcha-response']);

				if($recaptcha_result != TRUE) {
					$errors[] = $LNG['error_recaptcha'];
				}
			}

			if(!empty($errors)) {
				$this->printMessage(
					$LNG['error'],
					implode("<br>\r\n", $errors),
					NULL, array(array('label'	=> $LNG['error_back'], 'url'	=> 'javascript:window.history.back()',))
				);
			}

			//$test = $this->test(); COMING SOON

			$GLOBALS['DATABASE']->query("INSERT INTO ".VOTES." (`id_answer`, `ip_address`, `timestamp`) VALUES ('".$answer."', '".GeneralFunctions::getUserIP()."', '".TIMESTAMP."')");

			GeneralFunctions::redirectTo('index.php?page=poll&mode=results&id='.$id);

	}

	function results()
	{
		global $smarty;

		$id = GeneralFunctions::_GP('id', '');

		$query  	= $GLOBALS['DATABASE']->query("SELECT COUNT(*) AS count FROM ".POLLS." WHERE `id_poll` = ".$id."");
		$result 	= $GLOBALS['DATABASE']->fetch_array($query);

		if($result['count'] == 0) {
			$this->printMessage($LNG['error'], $LNG['poll_not_found'], NULL, array(array('label'	=> $LNG['error_back'], 'url'	=> 'javascript:window.history.back()',)));
		}

		$question = $GLOBALS['DATABASE']->getFirstRow("SELECT `question` FROM ".POLLS." WHERE `id_poll` = '".$id."'");

		$query = $GLOBALS['DATABASE']->query("SELECT * FROM ".ANSWERS." WHERE `id_poll` = '".$id."' ORDER BY `answer_order`");
		$array = array();

		while($answers = $GLOBALS['DATABASE']->fetch_array($query)) {
			$array[$answers['answer_order']] = $answers;
		}

		$array_results = array();
		$votes_count = 0;
		$i = 1;

		while($i <= count($array)) {

			$query 		= $GLOBALS['DATABASE']->query("SELECT COUNT(*) AS count FROM ".VOTES." WHERE `id_answer` = ".$array[$i]['id_answer']."");
			$result 	= $GLOBALS['DATABASE']->fetch_array($query);

			$array_results[$i] = $result['count'];
			$votes_count = $votes_count + $result['count'];

			$i++;
		}

		$smarty->assign(array(
			'question'				=> $question['question'],
			'array' 					=> $array,
			'array_results'		=> $array_results,
			'votes_count' 		=> $votes_count
		));

		$this->render('page.poll.results.tpl');
	}

	function test() {

	}

}
