<?php
define('MODE', 'INDEX');
define('ROOT_PATH', str_replace('\\', '/',dirname(__FILE__)).'/');
set_include_path(ROOT_PATH);

require 'includes/common.php';
require 'includes/pages/index/AbstractIndexPage.class.php';

$page 		= (empty($_GET['page'])) ? 'index' : $_GET['page'];
$mode 		= (empty($_GET['mode'])) ? 'show' : $_GET['mode'];
$page		= str_replace(array('_', '\\', '/', '.', "\0"), '', $page);
$pageClass	= 'Show'.ucfirst($page).'Page';

$path		= 'includes/pages/index/'.$pageClass.'.class.php';

require($path);

$pageObj	= new $pageClass;
$pageProps	= get_class_vars(get_class($pageObj));
$pageObj->{$mode}();
