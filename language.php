<?php
define('MODE', 'INDEX');
define('ROOT_PATH', str_replace('\\', '/',dirname(__FILE__)).'/');
set_include_path(ROOT_PATH);

require 'includes/common.php';

$language = GeneralFunctions::_GP('lang', 'en');
$LNG->setLanguage($language);

header('Refresh: 0; url='.$_SERVER['HTTP_REFERER']);
exit;
