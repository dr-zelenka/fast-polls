<?php
$LNG['main_title']                        = 'Anketní systém';

$LNG['error']                             = 'Chyba';
$LNG['error_back']                        = 'Zpět';

// MAIN PAGE

$LNG['inset_question']                    = 'Položte vaši otázku';
$LNG['insert_answer']                     = 'Napište možnost odpovědi';
$LNG['submit_poll']                       = 'Vytvořit anketu';
// Errors
$LNG['error_question']                    = 'Nevyplněná otázka!';
$LNG['error_answer']                      = 'Nevyplněná odpověď!';
$LNG['error_recaptcha']                   = 'Nevyplněná reCaptcha!';


// POLL

//default
$LNG['poll_vote']                         = 'Hlasovat';
$LNG['poll_results']                      = 'Výsledky';
$LNG['poll_not_found']                    = 'Anketa nenalezena!';
$LNG['poll_id_error']                     = 'To je ale divné ID!';
$LNG['poll_not_answer']                   = 'Nezvolil/a si odpověď!';
//results
$LNG['poll_votes']                        = 'Hlasovalo';
