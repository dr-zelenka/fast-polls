<?php
$LNG['main_title']                        = 'Polls system';

$LNG['error']                             = 'Error';
$LNG['error_back']                        = 'Back';

// MAIN PAGE

$LNG['inset_question']                    = 'Ask a question';
$LNG['insert_answer']                     = 'Enter option';
$LNG['submit_poll']                       = 'Create Poll';
// Errors
$LNG['error_question']                    = 'Unfilled question!';
$LNG['error_answer']                      = 'Unfilled odpověď!';
$LNG['error_recaptcha']                   = 'Unfilled reCATPCHA!';


// POLL

//default
$LNG['poll_vote']                         = 'Vote';
$LNG['poll_results']                      = 'Results';
$LNG['poll_not_found']                    = 'Polls not found!';
$LNG['poll_id_error']                     = 'This is a strange ID!';
$LNG['poll_not_answer']                   = 'You not select a answer!';
//results
$LNG['poll_votes']                        = 'Votes';
