{block name="title" prepend}Hlavní strana{/block}
{block name="content"}
<div id="error">
	<div class="title">{$title}</div>
	<div class="message">{$message}</div>
	{if !empty($redirectButtons)}
		<p>
			{foreach $redirectButtons as $button}
				<a href="{$button.url}"><button>{$button.label}</button></a>
			{/foreach}
		</p>
	{/if}
</div>
{/block}
