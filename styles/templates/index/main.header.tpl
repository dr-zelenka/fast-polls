<!DOCTYPE html>
<html lang="" class="no-js">
<head>
	<title>{block name="title"} • Anketka{/block}</title>
	<meta name="generator" content="">
	<meta name="keywords" content="Anketa">
	<meta name="description" content="Anketní systém">


	<!-- Zde jsou styleshity, tahám to z více souboru, protože nevidím důvod, proč ne -->
	<link rel="stylesheet" type="text/css" href="styles/css/index/main.css">
	<link rel="stylesheet" type="text/css" href="styles/css/index/results.css">
	<link rel="stylesheet" type="text/css" href="styles/css/index/poll.css">
	<link rel="stylesheet" type="text/css" href="styles/css/index/vote.css">
	<link rel="stylesheet" type="text/css" href="styles/css/index/error.css">

	<script src="scripts/Chart.bundle.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
<div id="languages_bar">
	<div class="languages_content">
		<div class="language_item">
			{foreach $languages as $language}
			<a href="language.php?lang={$language}"><img src="styles/images/flags/{$language}.png" alt="{$language}"></a>
			{/foreach}
		</div>
	</div>
	<div class="clear"></div>
</div>
