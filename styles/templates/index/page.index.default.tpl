{block name="title" prepend}Hlavní strana{/block}
{block name="content"}
<div id="page">
	<div id="poll">
		<form name="login" action="index.php?page=index&amp;mode=send" method="post">
			<div class="title">
				<div class="text">{$LNG.main_title}</div>
			</div>

			<div class="question">
					<input type="text" name="question" placeholder="{$LNG.inset_question}">
			</div>

			<div id="answer_row">
				<div class="answer">
					<input type="text" name="answer[]" placeholder="{$LNG.insert_answer}">
				</div>
			</div>
			<div id="answer_row">
				<div class="answer">
					<input type="text" name="answer[]" placeholder="{$LNG.insert_answer}">
				</div>
			</div>
			<div id="answer_row">
				<div class="answer">
					<input type="text" name="answer[]" placeholder="{$LNG.insert_answer}">
				</div>
			</div>

			{if $captcha_setting == true}
			<div class="g-recaptcha" data-sitekey="{$captcha_public}"></div>
			{/if}

			<div class="submit_button">
				<div><input type="submit" value="{$LNG.submit_poll}"></div>
			</div>

			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
			<script>
			function add_row()
			{
			 $("#answer_row").after("<div id='answer_row'><div class='answer'><input type='text' name='answer[]' placeholder='Napište možnost odpovědi'></div></div>");
			}
			</script>

		</form>
	</div>
</div>
{/block}
