{block name="title" prepend}Hlavní strana{/block}
{block name="content"}
<div id="page">

<div id="vote">
	<div class="title">
		<div class="text">{$LNG.main_title}</div>
	</div>

	<input type="text" value="{$poll_url}" readonly>

	<div class="question">
			{$question}
	</div>

	<form name="login" action="index.php?page=poll&amp;mode=send" method="post">
	<input type="hidden" name="id" value="{$id}">

	{foreach $array as $neco => $row}
	<label class="answer_container">{$row.answer}
	  <input type="radio" name="answer" value="{$row.id_answer}">
	  <span class="checkmark"></span>
	</label>
	{/foreach}

	{if $captcha_setting == true}
	<div class="g-recaptcha" data-sitekey="{$captcha_public}"></div>
	{/if}

	<div class="submit_button">
		<div><input type="submit" value="{$LNG.poll_vote}"></div>
	</div>

	<div class="results_button">
		<a href="index.php?page=poll&amp;mode=results&amp;id={$id}">{$LNG.poll_results}</a>
	</div>

	</form>

</div>

</div>
{/block}
