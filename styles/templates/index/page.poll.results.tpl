{block name="title" prepend}Hlavní strana{/block}
{block name="content"}
<div id="page">
	<div id="results">
		<div class="question_content">
			<div class="question_title">{$question}</div>
		</div>
		<div class="results_content">
			<div class="results_table">
				<div class="answer_column">
					{foreach $array as $neco => $row}
					<div class="answer_row">{$row.answer}</div>
					{/foreach}
					<div class="answer_row">{$LNG.poll_votes}</div>
				</div>
				<div class="count_column">
					{foreach $array_results as $neco => $row}
					<div class="count_row">{$row}</div>
					{/foreach}
					<div class="answer_row">{$votes_count}</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="chart_content">
			<div class="chart_block">
				<canvas id="pieChart" width="200" height="150"></canvas>
			</div>
		</div>
		<div class="clear"></div>
	</div>


	<script>
	var pieCanvas = document.getElementById("pieChart");

	Chart.defaults.global.defaultFontFamily = "Lato";
	Chart.defaults.global.defaultFontSize = 18;

	var pieData = {
	    labels: [{foreach $array as $neco => $row}"{$row.answer}",{/foreach}],
	    datasets: [
	        {
	            data: [{foreach $array_results as $neco => $row}{$row},{/foreach}],
	            backgroundColor: [
	                "#FF6384",
	                "#cc65fe",
	                "#36a2eb"
	            ]
	        }]
	};

	var pieChart = new Chart(pieCanvas, {
	  type: 'pie',
	  data: pieData,
		options: {
        legend: {
            display: false,
        }
		}
	});
	</script>

</div>
{/block}
